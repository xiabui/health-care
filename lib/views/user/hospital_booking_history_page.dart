//import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/models/book_details_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
//import 'package:carousel_slider/carousel_slider.dart';

class HospitalBookingHistoryPage extends StatefulWidget {
  @override
  _HospitalBookingHistoryPageState createState() => _HospitalBookingHistoryPageState();
}

class _HospitalBookingHistoryPageState extends State<HospitalBookingHistoryPage> {
  List<BookDetailsModel> _data = [];
  bool _isLoading = true;
  CollectionReference users =
  FirebaseFirestore.instance.collection('LichSuDat');

  @override
  void initState() {
    _getDataFromFirebase();

    super.initState();
  }


  _getDataFromFirebase() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String? userId = preferences.getString("user_id");

    await FirebaseFirestore.instance
        .collection('LichSuDat')
        .doc(userId)
        .collection("DanhSachChamSocTaiBV")
        .get()
        .then((QuerySnapshot querySnapshot) {
      print(querySnapshot.docs.length);
      for (int i = 0; i < querySnapshot.docs.length; i++) {
        _data.add(new BookDetailsModel(
            bookID: querySnapshot.docs[i].id,
            name: querySnapshot.docs[i]['name'],
            email: querySnapshot.docs[i]['email'],
            address: querySnapshot.docs[i]['address'],
            note: querySnapshot.docs[i]['note'],
            phone: querySnapshot.docs[i]['phone'],
            endTime: querySnapshot.docs[i]['end_time'].toString(),
            startTime: querySnapshot.docs[i]['start_time'].toString()));
      }
    }).whenComplete(() {
      setState(() {
        _isLoading = false;
      });
    });




  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Đơn chăm sóc tại nhà",
            style:
            TextStyle(color: (Colors.white), fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
          //backgroundColor: Colors.white,
        ),
        backgroundColor: Color.fromRGBO(234, 237, 237, 0.8),
        body: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0), color: Colors.grey),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: [
              _data.length > 0
                  ? _isLoading == false
                  ? Expanded(
                child: ListView.builder(
                    itemCount: _data.length,
                    itemBuilder: (context, index) => GestureDetector(
                      onTap: () {},
                      child: Card(
                        child: Column(
                          children: <Widget>[
                            Text(
                              "Tên khách hàng: " +
                                  _data[index].name!,
                              style:
                              TextStyle(color: Colors.green, fontSize: 18, fontWeight: FontWeight.bold),

                            ),
                            Text("Email: " + _data[index].email!,
                                style: TextStyle(
                                    color: Colors.green, fontSize: 18, fontWeight: FontWeight.bold)),
                            Text(
                                "Số điện thoại: " +
                                    _data[index].phone!,
                                style: TextStyle(
                                    color: Colors.green, fontSize: 18, fontWeight: FontWeight.bold)),
                            Text(
                                "Ngày giờ bắt đầu: " +
                                    _data[index].startTime!,
                                style: TextStyle(
                                    color: Colors.green, fontSize: 18, fontWeight: FontWeight.bold)),
                            Text(
                                "Ngày giờ kết thúc: " +
                                    _data[index].endTime!,
                                style: TextStyle(
                                    color: Colors.green, fontSize: 18, fontWeight: FontWeight.bold)),
                            Text(
                                "Địa chỉ: " +
                                    _data[index].address!,
                                style: TextStyle(
                                    color: Colors.green, fontSize: 18, fontWeight: FontWeight.bold)),
                            Text("Ghi chú: " + _data[index].note!,
                                style: TextStyle(
                                    color: Colors.green, fontSize: 18, fontWeight: FontWeight.bold)),

                            Container(
                              height: 25,
                            ),
                          ],
                        ),
                      ),
                    )),
              )
                  : CircularProgressIndicator()
                  : Center(
                child: Text("No data in database"),
              ),
            ],
          ),
        ));
  }
}
//
// Widget listItem(String image, String title, BuildContext context) {
//   return Container(
//     width: double.infinity,
//     margin: const EdgeInsets.fromLTRB(10, 5, 10, 5),
//     padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
//     decoration: BoxDecoration(
//         borderRadius: BorderRadius.circular(8.0), color: Colors.white),
//     child: Row(
//       children: [
//         Container(
//           height: 60,
//           width: 60,
//           decoration: BoxDecoration(
//             borderRadius: BorderRadius.circular(8.0),
//             image: DecorationImage(
//               image: AssetImage(image),
//               fit: BoxFit.cover,
//             ),
//           ),
//         ),
//         SizedBox(
//           width: 10,
//         ),
//         Container(
//           width: MediaQuery.of(context).size.width - 120,
//           child: Text(
//             title,
//             //Chỉnh style service
//             style: TextStyle(
//                 fontWeight: FontWeight.bold,
//                 fontSize: 20,
//                 color: Colors.green,
//                 fontFamily: 'Raleway'),
//             textAlign: TextAlign.justify,
//           ),
//         ),
//       ],
//     ),
//   );
// }