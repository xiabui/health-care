import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_app/views/user/home_page.dart';

class TricksTipsBookingPage extends StatefulWidget{
  @override
  _TricksTipsBookingPageState createState() => _TricksTipsBookingPageState();
}

class _TricksTipsBookingPageState extends State<TricksTipsBookingPage> {

  TextEditingController 
      _namectrl     = TextEditingController(),
      _emailctrl    = TextEditingController(), 
      _addressctrl  = TextEditingController(),
      _notectrl     = TextEditingController(), 
      _phonectrl    = TextEditingController(),
      _date         = TextEditingController(),
      _enddate      = TextEditingController();

  CollectionReference? ref;
  String _dateValue = "", _endDateValue = "";
  @override
  void initState(){
    final DateFormat formatter = DateFormat('dd/MM/yyyy H:m');
    setState(() {
      _dateValue = formatter.format(DateTime.now());
      _endDateValue = formatter.format(DateTime.now());
    });
    super.initState();
  }

  void submit(){
    Map<String, dynamic> _data = {
      //"title":"ChamSocTaiNha",
      "name": _namectrl.text,
      "phone": _phonectrl.text,
      "email": _emailctrl.text,
      "address": _addressctrl.text,
      "note": _notectrl.text,
      "start_time": _dateValue,
      "end_time": _endDateValue,
      "confirmed": false
    };
    ref = FirebaseFirestore.instance.collection("ChamSocTaiNha");
    ref!.add(_data).then((value){
      print("successfully");
    }).timeout(Duration(seconds: 15));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Chăm sóc tại nhà", style: TextStyle(
            color: (Colors.white), fontWeight: FontWeight.bold),),
        centerTitle: true,
        //backgroundColor: Colors.white,
      ),
      backgroundColor: Color.fromRGBO(255, 255, 255, 1.0),
      body: Container(

        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(

                margin: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                child:
                TextFormField(
                  controller: _namectrl,
                  decoration: const InputDecoration(
                    icon: Icon(Icons.person, color: Colors.green,),
                    labelText: 'Tên khách hàng',
                  ),
                  onSaved: (String? value) {
                    // This optional block of code can be used to run
                    // code when the user saves the form.
                  },

                  validator: (String? value) {
                    return (value != null && RegExp(r'^[a-zA-Z]+$').hasMatch(value)) ? 'Chỉ được nhập chữ' : null;
                  },
                ),
              ),

              Container(

                margin: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                child: TextFormField(
                  controller: _phonectrl,
                  keyboardType: TextInputType.number,
                  decoration: const InputDecoration(
                    icon: Icon(Icons.phone, color: Colors.green,),
                    labelText: 'Số điện thoại',
                  ),
                  onSaved: (String? value) {
                    // This optional block of code can be used to run
                    // code when the user saves the form.
                  },
                  validator: (String? value) {
                    return (value != null && value.length==10) ? 'Số điện thoại phải có đủ 10 số.' : null;
                  },
                ),
              ),


              Container(
                margin: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                child: TextFormField(
                  controller: _emailctrl,
                  decoration: const InputDecoration(
                    icon: Icon(Icons.email_outlined, color: Colors.green,),
                    labelText: 'Email',
                  ),
                  onSaved: (String? value) {
                    // This optional block of code can be used to run
                    // code when the user saves the form.
                  },
                  validator: (String? value) {
                    return (value != null && RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value)) ? 'Chưa đúng định dạng mail.' : null;
                  },
                ),
              ),

              Container(
                margin: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                child: DateTimePicker(
                  type: DateTimePickerType.dateTimeSeparate,
                  dateMask: 'dd/MM/yyyy',
                  initialValue: DateTime.now().toString(),
                  firstDate: DateTime(2000),
                  lastDate: DateTime(2100),
                  icon: Icon(Icons.event, color: Colors.green,),
                  dateLabelText: 'Ngay bat dau',
                  timeLabelText: "Gio bat dau",
                  use24HourFormat: true,
                  onChanged: (val){
                    setState(() {
                      _dateValue = val;
                      print(val);
                    });
                  },
                  onSaved: (val) => print(val),
                ),
              ),

              Container(
                margin: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                child: DateTimePicker(
                  type: DateTimePickerType.dateTimeSeparate,
                  dateMask: 'dd/MM/yyyy',
                  initialValue: DateTime.now().toString(),
                  firstDate: DateTime(2000),
                  lastDate: DateTime(2100),
                  icon: Icon(Icons.event, color: Colors.green,),
                  dateLabelText: 'Ngay ket thuc',
                  timeLabelText: "Gio ket thuc",
                  use24HourFormat: true,
                  onChanged: (val){
                    // Disable weekend days to select from the calendar
                    setState(() {
                      _endDateValue = val;
                    });

                  },
                  onSaved: (val) => print(val),
                ),
              ),

              Container(

                margin: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                child: TextFormField(
                  controller: _addressctrl,
                  minLines: 6,
                  maxLines: null,
                  keyboardType: TextInputType.multiline,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(

                    ),
                    icon: Icon(Icons.location_on_outlined, color: Colors.green,),
                    labelText: 'Địa chỉ',
                  ),
                  onSaved: (String? value) {
                    // This optional block of code can be used to run
                    // code when the user saves the form.
                  },
                  validator: (String? value) {
                    return (value != null) ? 'Chưa nhập địa chỉ.' : null;
                  },
                ),
              ),


              Container(
                margin: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                child: TextFormField(
                  controller: _notectrl,
                  minLines: 6,
                  maxLines: null,
                  keyboardType: TextInputType.multiline,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      icon: Icon(Icons.note_add_outlined, color: Colors.green,),
                      labelText: 'Ghi chú'

                  ),

                  onSaved: (String? value) {
                    // This optional block of code can be used to run
                    // code when the user saves the form.
                  },
                ),
              ),

              Container(
                  margin: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                  padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                  // ignore: deprecated_member_use
                  child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(30.0)),
                      color: Colors.green,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,

                        children: <Widget>[

                          SizedBox(width:20.0),
                          Text(
                            'Đặt lịch',
                            style: TextStyle(color: Colors.white,fontSize: 18.0,
                                fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                      onPressed:(){
                        submit();
                        print('done');
                        Navigator.push(
                          context,
                          new MaterialPageRoute(builder:(contex)=>new UserHomePage(),
                          ),
                        );
                      }
                  )
              ),


            ],
          ),
        ),
      ),

    );

  }
  void saveinfo() {
    String name = _namectrl.text;
    String email = _emailctrl.text;
    String address = _addressctrl.text;
    String note = _notectrl.text;
    String phone = _phonectrl.text;
    String date = _date.text;
    String time = _enddate.text;
    Map<String,String> booking = {
      'name':name,
      'email':email,
      'phone':'+84'+phone,
      'address':address,
      'note':note,
      'date':date,
      'time':time
    };
    //ref.push().set(booking);
  }
}

