import 'package:flutter/material.dart';
import 'package:flutter_app/views/user/tricks_tips_booking_page.dart';

// ignore: must_be_immutable
class TricksTipsPage extends StatelessWidget {

  List<ServiceModel> _list = [
    new ServiceModel(name:  "Thay băng và cắt chỉ, rửa 1 vết thương tại nhà", image: "assets/user/doctor.png",),
  ];

  var service = [
    "Thay băng và cắt chỉ, rửa 1 vết thương tại nhà",
    "Đặt sonde tiểu tại nhà",
    "Đặt sonde dạ dày tại nhà",
    "Chạy khí dung tại nhà",
    "Thụt tháo đại tràn tại nhà",
    "Hút đờm dãi cho trẻ nhỏ từ 6 - 12 tuổi tại nhà",
    "Hút đờm dãi cho người cao tuổi tại nhà",
    "Tắm gội cho bệnh nhân tại giường bệnh",
    "Rửa mũi cho trẻ sơ sinh và trẻ nhỏ",
  ];

  var image_thuthuat = [
    "assets/user/thuthuat.png",
    "assets/user/phuchoichucnang.png",
    "assets/user/xetnghiem.png",
    "assets/user/booking.png",
    "assets/user/thuthuat.png",
    "assets/user/phuchoichucnang.png",
    "assets/user/xetnghiem.png",
    "assets/user/booking.png",
    "assets/user/calendar.png"
  ];


  List<Widget>body=[
    TricksTipsBookingPage(),
    // FThuThuat(),
    // FThuThuat(),
    // FThuThuat(),
    // FThuThuat(),
    // FThuThuat(),
    // FThuThuat(),
    // FThuThuat(),
    // FThuThuat(),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Thủ thuật điều dưỡng", style: TextStyle(
              color: (Colors.white), fontWeight: FontWeight.bold),),
          centerTitle: true,
          //backgroundColor: Colors.white,
        ),
        backgroundColor: Color.fromRGBO(234, 237, 237, 0.8),
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: [
              Expanded(
                child: ListView.builder(
                  itemCount: service.length,
                  itemBuilder: (context, index){
                    return GestureDetector(
                        onTap: (){
                          Navigator.of(context).push(
                              MaterialPageRoute(builder:(route)=>body[index]
                              )
                          );
                          print('Tapped ${index}');
                        },
                        child: listItem(image_thuthuat[index], service[index], context)
                    );
                  },


                ),
              )
            ],
          ),
        )
    );
  }
}

Widget listItem(String image, String title, BuildContext context) {
  return Container(
    width: double.infinity,
    margin: const EdgeInsets.fromLTRB(10, 5, 10, 5),
    padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        color: Colors.white
    ),
    child: Row(
      children: [
        Container(
          height: 60,
          width: 60,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
            image: DecorationImage(
              image: AssetImage(image),
              fit: BoxFit.cover,
            ),
          ),
        ),

        SizedBox(width: 10,),

        Container(
          width: MediaQuery.of(context).size.width - 120,
          child: Text(
            title,
            //Chỉnh style service
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
                color: Colors.green,
                fontFamily: 'Raleway'
            ),
            textAlign: TextAlign.justify,
          ),
        ),
      ],
    ),
  );
}

class ServiceModel {
  ServiceModel({this.image, this.name});
  String? image;
  String? name;
}