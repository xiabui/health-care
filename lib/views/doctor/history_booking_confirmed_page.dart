import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/services/ggMap.dart';
import 'package:flutter_app/models/book_details_model.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BookConfirmHistoryPage extends StatefulWidget {
  @override
  _BookConfirmHistoryPageState createState() => _BookConfirmHistoryPageState();
}

class _BookConfirmHistoryPageState extends State<BookConfirmHistoryPage> {
  List<BookDetailsModel> _listData = [];
  bool _loadingState = true;

  @override
  void initState() {
    print("[SCREEN] Started screen BookConfirmHistoryPage.............");
    _getDataFromFirestore();
    super.initState();
  }

  _getDataFromFirestore() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String? userId = preferences.getString("user_id_doctor");

    await FirebaseFirestore.instance
        .collection("LichSuDat")
        .doc(userId)
        .collection("DanhSachXacNhan")
        .get()
        .then((QuerySnapshot? value) {
      if (value != null) {
        for (int i = 0; i < value.docs.length; i++) {
          _listData.add(new BookDetailsModel(
              bookID: value.docs[i].id,
              name: value.docs[i]['name'],
              email: value.docs[i]['email'],
              address: value.docs[i]['address'],
              note: value.docs[i]['note'],
              phone: value.docs[i]['phone'],
              endTime: value.docs[i]['end_time'],
              startTime: value.docs[i]['start_time'],
              position: value.docs[i]['position']));
        }
      }
    }).onError((error, stackTrace) {
      print('[ERROR] FirebaseError: $error');
    }).whenComplete(() {
      if (_listData != null) {
        print(_listData.length);
        setState(() {
          _loadingState = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Đơn đặt lịch",
            style:
                TextStyle(color: (Colors.white), fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
        ),
        backgroundColor: Color.fromRGBO(234, 237, 237, 0.8),
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: _loadingState == false
              ? Column(
                  children: [
                    Expanded(
                      child: ListView.builder(
                        itemCount: _listData.length,
                        itemBuilder: (context, index) =>
                            listItem(_listData[index], context),
                      ),
                    )
                  ],
                )
              : Center(
                  child: CircularProgressIndicator(),
                ),
        ));
  }
}

textRow(title, value) {
  return Container(
    margin: const EdgeInsets.only(top: 8),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: TextStyle(color: Colors.black, fontSize: 16),
        ),
        SizedBox(height: 2),
        Container(
          decoration: BoxDecoration(
            color: Colors.grey[100],
            borderRadius: BorderRadius.circular(2),
          ),
          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
          width: double.infinity,
          child: Text(
            value,
            style: TextStyle(
                color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold),
          ),
        ),
      ],
    ),
  );
}

Widget listItem(BookDetailsModel model, BuildContext context) {
  return Container(
    margin: const EdgeInsets.fromLTRB(10, 10, 10, 0),
    padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10), color: Colors.white),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        textRow('Tên khách hàng: ', model.name!),
        textRow('Email: ', model.email!),
        textRow("Số điện thoại: ", model.phone!),
        textRow("Ngày giờ bắt đầu: ", '${model.startTime}'),
        textRow("Ngày giờ kết thúc: ", '${model.endTime}'),
        textRow("Địa chỉ: ", model.address!),
        textRow("Ghi chú: ", model.note!),
        Container(
          height: 15,
        ),
        GestureDetector(
            onTap: () {
              print("History: ${model.position!.latitude}");
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (route) => MapDirectionPage(
                          targetPosition: Position(
                        latitude: model.position!.latitude,
                        longitude: model.position!.longitude,
                        timestamp: DateTime.now(),
                        heading: 0,
                        speedAccuracy: 0,
                        accuracy: 0,
                        altitude: 0,
                        speed: 0,
                      ))));
            },
            child: Container(
                width: double.infinity,
                height: 50,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.0),
                    color: Colors.green),
                child: const Text(
                  "CHỈ ĐƯỜNG",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                ))),
        Container(
          height: 5,
        ),
      ],
    ),
  );
}
