import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_app/views/doctor/doctor_home_page.dart';
import 'package:shared_preferences/shared_preferences.dart';



class LoginPage extends StatefulWidget {
  LoginPage ({Key? key}): super(key:key);
  @override

  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
   TextEditingController emailController = TextEditingController();
   TextEditingController passwordController = TextEditingController();

   signIn() async {
     SharedPreferences pref = await SharedPreferences.getInstance();

     String? email = emailController.text;
     String? password = passwordController.text;

     if(email != null && password != null) {
       await FirebaseAuth.instance.signInWithEmailAndPassword(email: email, password: password).then((value){
         if(value != null) {
           pref.setString("user_id_doctor", value.user!.uid.toString());
           print(value);
           Navigator.push(
             context,
             MaterialPageRoute(
               builder: (context) => DoctorHomePage(),
             ),
           );
         }
       }).onError((error, stackTrace){
         print("Error: $error");
       }).timeout(Duration(seconds: 15));
     }

   }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(

        ),
        body: Padding(
            padding: EdgeInsets.all(10),
            child: ListView(
              children: <Widget>[
                Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(10),
                    child: Text(
                      'Hearth-Care',
                      style: TextStyle(
                          color: Colors.green,
                          fontWeight: FontWeight.bold,
                          fontSize: 30),
                    )),
                Container(
                  padding: EdgeInsets.all(10),
                  child: TextFormField(
                    // onChanged: (value){
                    //   _email = value;
                    // },
                    controller: emailController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(30)),
                      labelText: 'Emai',
                    ),
                    validator: (String? value) {
                      return (value != null && RegExp(
                          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                          .hasMatch(value))
                          ? 'Chưa đúng định dạng mail.'
                          : null;
                          }
                    )
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                  child: TextFormField(
                    // onChanged: (value){
                    //   _password = value;
                    // },
                    obscureText: true,
                    controller: passwordController,
                    decoration: InputDecoration(

                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(30)),
                      labelText: 'Mật khẩu',
                    ),
                  ),
                ),

                Container(
                    height: 50,
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),

                    // ignore: deprecated_member_use
                    child: RaisedButton(
                      textColor: Colors.white,
                      color: Colors.green,
                      shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30)),
                      child: Text('Đăng nhập',style: TextStyle(fontWeight: FontWeight.bold,
                      fontSize: 20),),
                      onPressed: (){
                        signIn();
                      },
                    )),
              ],
            )));
  }

}