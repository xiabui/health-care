import 'dart:async';
import 'package:flutter_app/views/doctor/doctor_home_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_app/views/general/login_page.dart';
import 'package:flutter_app/views/user/home_page.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';

GoogleSignIn _googleSignIn = GoogleSignIn.standard();

class LoginWithGooglePage extends StatefulWidget {
  @override
  _LoginWithGooglePageState createState() => _LoginWithGooglePageState();
}

class _LoginWithGooglePageState extends State<LoginWithGooglePage> {
  @override
  void initState() {
    isSignIn();
    super.initState();
  }

  Future<void> isSignIn() async {
    SharedPreferences? preferences = await SharedPreferences.getInstance();

    if (preferences.getString("user_id") != null) {
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (route) => UserHomePage()));
    }

    if (preferences.getString("user_id_doctor") != null) {
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (route) => DoctorHomePage()));
    }
  }

  saveData(GoogleSignInAccount account) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString("user_id", account.id);
    preferences.setString("user_email", account.email);
    preferences.setString("user_photo", account.photoUrl!);
    preferences.setString("user_name", account.displayName!);
  }

  onSignInPress() async {
    await _googleSignIn.signOut();
    await _googleSignIn
        .signIn()
        .then((value) => {
              if (value != null)
                {
                  saveData(value),
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => UserHomePage()))
                }
              // ignore: missing_return
            })
        // ignore: unnecessary_brace_in_string_interps
        .catchError((value) {
      print(value);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Builder(
        builder: (context) => Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Image.network(
                  'https://images.unsplash.com/photo-1518050947974-4be8c7469f0c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
                  fit: BoxFit.fill,
                  color: Color.fromRGBO(255, 255, 255, 0.6),
                  colorBlendMode: BlendMode.modulate),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: 15.0),
                Container(
                    width: 280.0,
                    child: Align(
                      alignment: Alignment.center,
                      // ignore: deprecated_member_use
                      child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0)),
                          color: Color(0xffffffff),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Icon(
                                FontAwesomeIcons.google,
                                color: Color(0xffCE107C),
                              ),
                              SizedBox(width: 10.0),
                              Text(
                                'Sign in with Google',
                                style: TextStyle(
                                    color: Colors.black, fontSize: 18.0),
                              ),
                            ],
                          ),
                          onPressed: () {
                            onSignInPress();
                          }),
                    )),
                Container(
                    width: 280.0,
                    child: Align(
                      alignment: Alignment.center,
                      // ignore: deprecated_member_use
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0)),
                        color: Color(0xffffffff),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              FontAwesomeIcons.facebookF,
                              color: Color(0xff4754de),
                            ),
                            SizedBox(width: 10.0),
                            Text(
                              'Sign in with Facebook',
                              style: TextStyle(
                                  color: Colors.black, fontSize: 18.0),
                            ),
                          ],
                        ),
                        onPressed: () {},
                      ),
                    )),
                Container(
                    child: Row(
                  children: <Widget>[
                    //Text('Bạn là điều dưỡng, bác sĩ?',style: TextStyle(fontWeight: FontWeight.bold),),
                    // ignore: deprecated_member_use
                    FlatButton(
                      textColor: Colors.white,
                      child: Text(
                        'Bạn là bác sĩ, điều dưỡng?',
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      onPressed: () {
                        //signup screen
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (contex) => DoctorHomePage(),
                          ),
                        );
                      },
                    )
                  ],
                  mainAxisAlignment: MainAxisAlignment.center,
                )),
                Container(
                    width: 210.0,
                    child: Align(
                      alignment: Alignment.center,
                      // ignore: deprecated_member_use
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0)),
                        color: Color(0xffffffff),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(width: 10.0),
                            Text(
                              'Đăng nhập tại đây',
                              style: TextStyle(
                                  color: Colors.green, fontSize: 18.0),
                            ),
                          ],
                        ),
                        onPressed: () {
                          Navigator.push(
                            context,
                            new MaterialPageRoute(
                              builder: (contex) => new LoginPage(),
                            ),
                          );
                        },
                      ),
                    )),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
