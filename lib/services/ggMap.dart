import 'dart:async';
import 'package:flutter_app/models/direction_model.dart';
import 'package:flutter_app/services/directions_respository.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

class MapDirectionPage extends StatefulWidget {
  MapDirectionPage({required this.targetPosition});
  final Position targetPosition;
  @override
  _MapDirectionPageState createState() => _MapDirectionPageState();
}

class _MapDirectionPageState extends State<MapDirectionPage> {
  GoogleMapController? _controller;
  double _originLatitude = 6.5212402, _originLongitude = 3.3679965;
  double _destLatitude = 6.849660, _destLongitude = 3.648190;
  Map<PolylineId, Polyline> _polylines = {};
  List<LatLng> polylineCoordinates = [];
  PolylinePoints polylinePoints = PolylinePoints();
  String googleAPIKey = "AIzaSyBSFNxIJUolJds8e4braq4qTVrivpDeWXQ";
  CameraPosition? initialLocation;

  //Current position
  Position? position;
  bool loading = true;

  Color _directionColor = Colors.red;
  Directions? _info;
  Marker? _origin;
  Marker? _destination;

  @override
  void initState() {
    print("Hello");
    _determinePosition().then((value) {
      if (value != null) {
        setState(() {
          position = value;
        });
      }
    }).whenComplete(() {
      if (position != null) {
        _getDirections().whenComplete(() {
          if (_info?.polylinePoints != null) {
            setState(() {
              loading = false;
            });
          }
        });
      }
    });

    super.initState();
  }

  Future<void> _getDirections() async {
    setState(() {
      _origin = Marker(
        markerId: const MarkerId('origin'),
        infoWindow: const InfoWindow(title: 'Origin'),
        icon: BitmapDescriptor.defaultMarker,
        position: LatLng(position!.latitude, position!.longitude),
      );

      _destination = Marker(
        markerId: const MarkerId('destination'),
        infoWindow: const InfoWindow(title: 'Destination'),
        icon: BitmapDescriptor.defaultMarker,
        position: LatLng(
            widget.targetPosition.latitude, widget.targetPosition.longitude),
      );
    });

    final directions = await DirectionsRespository().getDirections(
      origin: LatLng(position!.latitude, position!.longitude),
      destination: LatLng(
          widget.targetPosition.latitude, widget.targetPosition.longitude),
    );

    setState(() {
      _info = directions;
    });
  }

  void onMapCreated(GoogleMapController controller) async {
    _controller = controller;
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }
    return await Geolocator.getCurrentPosition();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "BlueCare - Map",
            style:
                TextStyle(color: (Colors.white), fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
        ),
        body: Container(
            child: loading == false
                ? GoogleMap(
                    initialCameraPosition: CameraPosition(
                        target: LatLng(position!.latitude, position!.longitude),
                        zoom: 15),
                    myLocationEnabled: true,
                    tiltGesturesEnabled: true,
                    compassEnabled: true,
                    scrollGesturesEnabled: true,
                    zoomGesturesEnabled: true,
                    onMapCreated: onMapCreated,
                    markers: {
                      if (_origin != null) _origin!,
                      if (_destination != null) _destination!
                    },
                    polylines: {
                      if (_info != null)
                        Polyline(
                            polylineId: const PolylineId("overview_polyline"),
                            color: _directionColor,
                            width: 5,
                            points: _info!.polylinePoints!
                                .map((e) => LatLng(e.latitude, e.longitude))
                                .toList()),
                    },
                  )
                : Center(child: CircularProgressIndicator())));
  }
}
